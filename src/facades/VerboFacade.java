package facades;

import beans.Verbo;
import hibernate.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *  @netbeans.hibernate.facade beanClass=Verbo
 */
public class VerboFacade { 
    
    private Session session;
    
    public VerboFacade(){
        session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void saveVerbo(Verbo verbo) {
        Transaction tx = session.beginTransaction();
        try{
            if(verbo.getIdverbo()==-1){
                session.save(verbo);
                tx.commit();
            }
            else{
                Verbo ver=(Verbo)session.load(Verbo.class,new Integer(verbo.getIdverbo()));
                ver.setEsp(verbo.getEsp());
                ver.setPre(verbo.getPre());
                ver.setPas(verbo.getPas());
                ver.setPar(verbo.getPar());
                ver.setGer(verbo.getGer());
                session.update(ver);
                tx.commit();
            }
        }catch(Exception e){tx.rollback();}
    }    
    public void deleteVerbo(int idverbo){
        Transaction tx = session.beginTransaction();
        try{
            Verbo ver=(Verbo)session.load(Verbo.class,new Integer(idverbo));
            session.delete(ver);
            tx.commit();
        }catch(Exception e){tx.rollback();}
    }
    public Verbo findById(int verboIdverbo) {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.idverbo = ? ");
        query.setInteger(0,verboIdverbo);
        return (Verbo) query.uniqueResult();
    }
    public Verbo findByEsp(java.lang.String verboEsp) {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.esp = ? ");
        query.setParameter(0,verboEsp);
        return (Verbo) query.uniqueResult();
    }
    public Verbo findByPre(java.lang.String verboPre) {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.pre = ? ");
        query.setParameter(0,verboPre);
        return (Verbo) query.uniqueResult();
    }
    public Verbo findByPas(java.lang.String verboPas) {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.pas = ? ");
        query.setParameter(0,verboPas);
        return (Verbo) query.uniqueResult();
    }
    public Verbo findByPar(java.lang.String verboPar) {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.par = ? ");
        query.setParameter(0,verboPar);
        return (Verbo) query.uniqueResult();
    }
    public Verbo findByGer(java.lang.String verboGer) {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.ger = ? ");
        query.setParameter(0,verboGer);
        return (Verbo) query.uniqueResult();
    }
    public java.util.List findAll() {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                " order by verbo.pre ");
        return query.list();
    }
    public java.util.List findRegulares() {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.reg = 0 " +
                " order by verbo.pre ");
        return query.list();
    }
    public java.util.List findIrregulares() {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.reg = 1 " +
                " order by verbo.pre ");
        return query.list();
    }
    public int findAllNum() {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                " order by verbo.pre ");
        return query.list().size();
    }
    public int findRegularesNum() {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.reg = 0 " +
                " order by verbo.pre ");
        return query.list().size();
    }
    public int findIrregularesNum() {
        org.hibernate.Query query = session.createQuery(
                " select verbo " +
                " from  " +
                " Verbo as verbo " +
                "  where  " +
                " verbo.reg = 1 " +
                " order by verbo.pre ");
        return query.list().size();
    }
    
    public void close(){
        session.clear();
        session.close();
    }
    
}
